/**
 * Entry point
 */
import React, { Component, Fragment } from 'react';
import { render } from 'react-dom';
import { PageHeader, PageHero, PageFeatures, PageMap } from './components';
import './styles/index.css';

const LANGUAGES = {
  uk: 'United Kingdom',
  no: 'Norway',
  se: 'Sweden',
  fi: 'Finland',
  dk: 'Denmark',
  de: 'Germany',
  nl: 'Netherlands',
};

class Page extends Component {
  state = {
    isHeader: false,
    language: 'uk',
  }

  componentDidMount() {
    window.scrollTo(0, 0);
    window.document.addEventListener('scroll', this.handleScroll);
    this.mediaQueryListener = window.matchMedia('(max-width: 640px)');
    this.mediaQueryListener.addListener(this.handleMediaQueryChange);
    this.handleMediaQueryChange();
    this.updateStyles();
  }

  componentWillUnmount() {
    window.document.removeEventListener('scroll', this.handleScroll);
    this.mediaQueryListener.removeListener(this.handleMediaQueryChange);
  }

  handleMediaQueryChange = () => {
    this.isMobile = this.mediaQueryListener.matches;
    if (this.isMobile) {
      this.hero.phone.style.transform = 'translateY(0)';
    }
  }

  handleScroll = () => {
    const delta = window.scrollY;

    if (delta < 0 || delta > 1000) {
      return;
    }

    const { isHeader } = this.state;

    if (delta > 200 && !isHeader) {
      this.setState({ isHeader: true });
    } else if (delta < 200 && isHeader) {
      this.setState({ isHeader: false });
    }

    window.requestAnimationFrame(this.updateStyles);
  }

  updateStyles = () => {
    const delta = window.scrollY;
    this.hero.cover.style.transform = `scale(${1 + (delta / 4000)})`;
    this.hero.cover.style.opacity = 1 - (delta / 700);
    if (!this.isMobile) {
      this.hero.phone.style.transform = `translateY(${delta / 5}px) scale(${1 + (delta / 5000)})`;
    }
  }

  changeLanguage = language => this.setState({ language });

  render() {
    const { isHeader, language } = this.state;
    return (
      <Fragment>
        <PageHeader
          on={isHeader}
          languages={LANGUAGES}
          currentLanguage={language}
          changeLanguage={this.changeLanguage}
        />
        <PageHero
          ref={ref => this.hero = ref}
          languages={LANGUAGES}
          currentLanguage={language}
          changeLanguage={this.changeLanguage}
        />
        <PageFeatures />
        <PageMap />
      </Fragment>
    );
  }
}

render(
  <Page />,
  document.getElementById('loop-rocks-root'),
);
