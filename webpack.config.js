const { resolve } = require('path');
const webpack = require('webpack');

const isDev = process.env.NODE_ENV === 'development' || false;
const buildPath = resolve(__dirname, 'build');
const nodeModules = resolve(__dirname, 'node_modules');

/* eslint-disable global-require */
const postcssPlugins = [
  require('postcss-partial-import'),
  require('stylelint'),
  require('precss'),
  require('postcss-calc'),
  require('autoprefixer'),
];
/* eslint-enable global-require */

const config = {
  entry: [
    'babel-polyfill',
    resolve(__dirname, 'index.js'),
  ],

  output: {
    path: buildPath,
    publicPath: '/',
    filename: 'bundle.js',
  },

  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: nodeModules,
        loader: 'eslint-loader',
        enforce: 'pre',
      },
      {
        test: /\.js$/,
        exclude: nodeModules,
        loader: 'babel-loader',
      },
      {
        test: /\.css$/,
        use: [
          { loader: 'style-loader' },
          { loader: 'css-loader' },
          {
            loader: 'postcss-loader',
            options: {
              sourceMap: true,
              plugins: postcssPlugins,
            },
          },
        ],
      },
      {
        test: /\.svg$/,
        loader: 'svg-url-loader',
      },
      {
        test: /\.(jpe?g|png)$/i,
        loader: 'url-loader',
      },
    ],
  },

  plugins: [
    new webpack.DefinePlugin({
      'process.env': { NODE_ENV: JSON.stringify(process.env.NODE_ENV) },
    }),
    new webpack.HotModuleReplacementPlugin(),
  ],

  resolve: {
    extensions: ['.js'],
    modules: [nodeModules],
  },
};

if (isDev) {
  config.devtool = 'eval-source-map';
  config.plugins.push(new webpack.HotModuleReplacementPlugin());
  config.devServer = {
    hot: true,
    port: 8080,
    contentBase: buildPath,
    inline: true,
  };
}

module.exports = config;
