/**
 * PageFeatures Component
 */
import React from 'react';
import './styles.css';

const PageFeatures = () =>
  <div className="page-features">
    <div className="page-features-inner">
      <div className="page-features-col">
        <b>7275</b> People using Loop Rocks
      </div>
      <div className="page-features-col">
        <b>287 424 ton</b> Masses available
      </div>
      <div className="page-features-col">
        <b>604 849 ton</b> Masses in demand
      </div>
    </div>
  </div>;

export default PageFeatures;
