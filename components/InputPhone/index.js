/**
 * InputPhone Component
 */
import React from 'react';
import { DropdownCountries } from '../../components';
import './styles.css';

const InputPhone = () =>
  <div className="input-phone uk">
    <DropdownCountries />
    <input
      type="text"
      className="input-phone-input"
      placeholder="Enter your phone"
    />
    <button
      type="button"
      className="input-phone-button"
    >
      Text me a link
    </button>
  </div>;

export default InputPhone;
