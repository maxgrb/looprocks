/**
 * PageHero Component
 */
import React from 'react';
import GoogleMap from 'google-map-react';
import './styles.css';

const PageHero = () =>
  <div className="page-map">
    <GoogleMap
      apiKey="AIzaSyD0Vo_ZOkMPJdKG815f4yEccvCEOeTcyyc"
      center={[63, 15]}
      zoom={5}
    />
  </div>;

export default PageHero;
