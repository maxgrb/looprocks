/**
 * DropdownLanguages Component
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import enhanceWithClickOutside from 'react-click-outside';
import './styles.css';

class DropdownLanguages extends Component {
  static propTypes = {
    languages: PropTypes.object.isRequired,
    currentLanguage: PropTypes.string.isRequired,
    changeLanguage: PropTypes.func.isRequired,
  };

  state = {
    open: false,
  };

  get body() {
    if (!this.state.open) {
      return undefined;
    }

    const { currentLanguage, languages } = this.props;

    return (
      <div className="dropdown-languages-body">
        {Object.keys(languages)
          .filter(key => key !== currentLanguage)
          .map(key => (
            <button
              type="button"
              key={key}
              value={key}
              onClick={this.handleClick}
              className={`dropdown-languages-item ${key}`}
            >
              {languages[key]}
            </button>
        ))}
      </div>
    );
  }

  toggle = () => this.setState({ open: !this.state.open });

  close = () => this.setState({ open: false });

  handleClickOutside = this.close;

  handleClick = (e) => {
    this.close();
    this.props.changeLanguage(e.target.value);
  };

  render() {
    const { languages, currentLanguage } = this.props;
    return (
      <div className="dropdown-languages">
        <button
          type="button"
          className={`dropdown-languages-button ${currentLanguage}`}
          onClick={this.toggle}
        >
          {languages[currentLanguage]}
        </button>
        {this.body}
      </div>);
  }
}

export default enhanceWithClickOutside(DropdownLanguages);
