/**
 * PageHero Component
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { DropdownLanguages, InputPhone } from '../../components';
import './styles.css';

export default class PageHero extends Component {
  static propTypes = {
    languages: PropTypes.object.isRequired,
    currentLanguage: PropTypes.string.isRequired,
    changeLanguage: PropTypes.func.isRequired,
  };

  render() {
    const { languages, currentLanguage, changeLanguage } = this.props;

    return (
      <div className="page-hero">
        <div
          className="page-hero-cover"
          ref={ref => this.cover = ref}
        />
        <div className="page-hero-inner">
          <a
            href="#"
            className="page-hero-logo"
            alt="Loop Rocks"
          >LoopRocks
          </a>
          <h1>
            Do you need or do you want to get rid of fill materials, gravel, or soil?
          </h1>
          <div
            className="page-hero-phone"
            ref={ref => this.phone = ref}
          />
          <DropdownLanguages
            languages={languages}
            currentLanguage={currentLanguage}
            changeLanguage={changeLanguage}
          />
          <ol>
            <li>Create an ad for FREE outlining what you need</li>
            <li>Choose the solution that suits you best and save money</li>
            <li>It’s easy! We help with testing and billing</li>
          </ol>
          <p>Send an SMS with a link to the app!</p>
          <InputPhone />
        </div>
      </div>);
  }
}
