/**
 * PageHeader Component
 */
import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { DropdownLanguages } from '../../components';
import './styles.css';

const PageHeader = ({
  on, languages, currentLanguage, changeLanguage,
}) =>
  <div className={classNames('page-header', { on })}>
    <div className="page-header-inner">
      <a
        href="#"
        className="page-header-logo"
        alt="Loop Rocks"
      >
        LoopRocks
      </a>
      <DropdownLanguages
        languages={languages}
        currentLanguage={currentLanguage}
        changeLanguage={changeLanguage}
      />
    </div>
  </div>;

PageHeader.propTypes = {
  on: PropTypes.bool.isRequired,
  languages: PropTypes.object.isRequired,
  currentLanguage: PropTypes.string.isRequired,
  changeLanguage: PropTypes.func.isRequired,
};

export default PageHeader;
