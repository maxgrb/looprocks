# Test for Loop Rocks

The repositoy contains technical test for Loop Rocks. It's a small web app, built with React, which render a simple page and a map with data. Also it contains a basic config for webpack, which helps compile all sources together.

## How to run

    Open `build/index.html`

## How to build

1. Install [Node.js](https://nodejs.org/en/) and [Yarn](https://yarnpkg.com).
2. Run `yarn` to install dependencies.
3. Then `yarn dev` to start development server on `localhost:8080` or `yarn build` – to get the build in `/build` directory.
